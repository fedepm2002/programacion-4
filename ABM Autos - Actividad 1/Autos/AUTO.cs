﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autos
{
    class AUTO
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }


        private string patente;

        public string Patente
        {
            get { return patente; }
            set { patente = value; }
        }

        private COLOR color;

        public COLOR Color
        {
            get { return color; }
            set { color = value; }
        }

        private MARCA marca;

        public MARCA Marca
        {
            get { return marca; }
            set { marca = value; }
        }



    }
}
