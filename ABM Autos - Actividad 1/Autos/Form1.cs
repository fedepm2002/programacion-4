﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

using System.Data.SqlClient;

using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autos
{
    public partial class Form1 : Form
    {
        AccesoDatos accesodatos = new AccesoDatos();
        public Form1()
        {
            InitializeComponent();
        }

        private void Leer_Click(object sender, EventArgs e)
        {
            
            
        }

        private void Enviar_Click(object sender, EventArgs e)
        {

            string consulta = "INSERT INTO AUTO (PATENTE, ID_MARCA, ID_COLOR) values ('" + textBox1.Text + "', " + (comboBox2.SelectedIndex+1) + ", " + (comboBox1.SelectedIndex+1) + ")";

            AccesoDatos acceso = new AccesoDatos();

            int resultado = acceso.Escribir(consulta);

            if (resultado == -1)
            {
                MessageBox.Show("Ha ocurrido un error", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Se aplicaron los cambios.");
            }

            accesodatos.ListarAutos();
            GC.Collect();
            dataGridViewGeneral.DataSource = null;
            dataGridViewGeneral.DataSource = accesodatos.ListarAutos();
        }

        private void Borrar_Click(object sender, EventArgs e)
        {
            AccesoDatos acceso = new AccesoDatos();

            foreach(DataGridViewRow registro in dataGridViewGeneral.SelectedRows)
            {
                AUTO auto = (AUTO) registro.DataBoundItem;

                string sql = "DELETE FROM AUTO WHERE ID =" + auto.ID;

                acceso.Escribir(sql);
            }

            acceso = null;

            accesodatos.ListarAutos();

            GC.Collect();
            dataGridViewGeneral.DataSource = null;
            dataGridViewGeneral.DataSource = accesodatos.ListarAutos();
        }

        void EnlazarAutos()
        {
            dataGridViewGeneral.DataSource = null;
            dataGridViewGeneral.DataSource = accesodatos.ListarAutos();
        }

        void EnlazarColores()
        {
            comboBox1.DataSource = null;
            comboBox1.DataSource = accesodatos.ListarColores();
        }

        void EnlazarMarcas()
        {
            comboBox2.DataSource = null;
            comboBox2.DataSource = accesodatos.ListarMarcas();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            EnlazarColores();
            EnlazarAutos();
            EnlazarMarcas();
        }

        private void dataGridViewGeneral_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            AUTO auto = (AUTO)dataGridViewGeneral.Rows[e.RowIndex].DataBoundItem;

            PatenteLabel.Text = auto.Patente;
            textBox1.Text = auto.Patente;

            string color = auto.Color.ToString();
            comboBox1.Text = color;

            string marca = auto.Marca.ToString();
            comboBox2.Text = marca;
        }

        private void EditarBoton_Click(object sender, EventArgs e)
        {
            COLOR color = (COLOR)comboBox1.SelectedItem;
            MARCA marca = (MARCA)comboBox2.SelectedItem;
            String sql;

            sql = "UPDATE AUTO SET PATENTE ='" + textBox1.Text;
            sql += "', ID_MARCA= " + marca.ID.ToString() + ", ID_COLOR= " + color.ID.ToString() + " WHERE PATENTE= '" + PatenteLabel.Text + "'";

            if (accesodatos.Escribir(sql) == -1)
            {
                MessageBox.Show("Ocurrió un error", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("OK");
            }
            EnlazarAutos();

        }

        private void AñadirColorBoton_Click(object sender, EventArgs e)
        {
            string sql = "INSERT INTO COLOR (color) VALUES ('" + textBox2.Text + "')";

            if (accesodatos.Escribir(sql) == -1)
            {
                MessageBox.Show("Ocurrió un error", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Se ha agregado un color");
            }

            EnlazarColores();
        }

        private void AñadirMarcaBoton_Click(object sender, EventArgs e)
        {
            string sql = "INSERT INTO MARCA (marca) VALUES ('" + textBox3.Text + "')";

            if (accesodatos.Escribir(sql) == -1)
            {
                MessageBox.Show("Ocurrió un error", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("Se ha agregado una marca");
            }

            EnlazarMarcas();
        }
    }
}
