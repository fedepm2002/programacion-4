﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Autos
{
    class AccesoDatos
    {
        private SqlConnection conexion;

        public AccesoDatos()
        {
            conexion = new SqlConnection();

            conexion.ConnectionString = @"Data source=.\sqlexpress; Initial Catalog=Actividad1ProgIV; Integrated Security= SSPI;";
        }

        private void Abrir()
        {
            conexion.Open();
        }

        private void Cerrar()
        {
            conexion.Close();
        }
            
        public int Escribir(string sql)
        {
            int filasAfectadas = 0;
            Abrir();

            SqlCommand comando = CrearComando(sql);

            try
            {
                filasAfectadas = comando.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                filasAfectadas = -1;
            }


            Cerrar();

            return filasAfectadas;
        }

        private SqlCommand CrearComando(string sql)
        {
            SqlCommand comando = new SqlCommand();

            comando.CommandText = sql;

            comando.Connection = conexion;

            comando.CommandType = CommandType.Text;

            return comando;
        }

        public List<COLOR> ListarColores()
        {
            List<COLOR> colores = new List<COLOR>();

            Abrir();

            string sql = "SELECT * FROM COLOR";

            SqlCommand comando = CrearComando(sql);

            SqlDataReader lector = comando.ExecuteReader();

            while (lector.Read())
            {
                COLOR color = new COLOR();
                color.ID = int.Parse(lector["id_color"].ToString());
                color.Color = lector["Color"].ToString();

                colores.Add(color);
            }

            lector.Close();

            Cerrar();
            return colores;
        }

        public List<MARCA> ListarMarcas()
        {
            List<MARCA> marcas = new List<MARCA>();

            Abrir();

            string sql = "SELECT * FROM MARCA";

            SqlCommand comando = CrearComando(sql);

            SqlDataReader lector = comando.ExecuteReader();

            while (lector.Read())
            {
                MARCA marca = new MARCA();
                marca.ID = int.Parse(lector["id_marca"].ToString());
                marca.Marca = lector["Marca"].ToString();

                marcas.Add(marca);
            }

            lector.Close();

            Cerrar();
            return marcas;
        }

        public List<AUTO> ListarAutos()
        {
            List<AUTO> autos = new List<AUTO>();

            List<COLOR> colores = ListarColores();

            List<MARCA> marcas = ListarMarcas();

            Abrir();

            string sql = "SELECT * FROM AUTO";
            SqlCommand comando = CrearComando(sql);

            SqlDataReader lector = comando.ExecuteReader();

            while (lector.Read())
            {
                AUTO auto = new AUTO();
                auto.ID = int.Parse(lector["ID"].ToString());
                auto.Patente = lector["PATENTE"].ToString();
                int indice = 0;

                while (auto.Color == null)
                {
                    if (int.Parse(lector["ID_COLOR"].ToString()) == colores[indice].ID)
                    {
                        auto.Color = colores[indice];
                    }
                    else
                    {
                        indice++;
                    }
                }

                indice = 0;

                while (auto.Marca == null)
                {
                    if (int.Parse(lector["ID_MARCA"].ToString()) == marcas[indice].ID)
                    {
                        auto.Marca = marcas[indice];
                    }
                    else
                    {
                        indice++;
                    }
                }

                autos.Add(auto);
            }

            lector.Close();
            Cerrar();
            return autos;
        }
    }
}
