﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Autos
{
    class MARCA
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private string marca;

        public string Marca
        {
            get { return marca; }
            set { marca = value; }
        }

        public override string ToString()
        {
            return marca;
        }
    }
}
