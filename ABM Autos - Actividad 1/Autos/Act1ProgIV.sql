USE [master]
GO
/****** Object:  Database [Actividad1ProgIV]    Script Date: 28/8/2021 12:10:37 ******/
CREATE DATABASE [Actividad1ProgIV]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Actividad1ProgIV', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Actividad1ProgIV.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Actividad1ProgIV_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Actividad1ProgIV_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Actividad1ProgIV] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Actividad1ProgIV].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Actividad1ProgIV] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET ARITHABORT OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Actividad1ProgIV] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Actividad1ProgIV] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Actividad1ProgIV] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Actividad1ProgIV] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Actividad1ProgIV] SET  MULTI_USER 
GO
ALTER DATABASE [Actividad1ProgIV] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Actividad1ProgIV] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Actividad1ProgIV] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Actividad1ProgIV] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Actividad1ProgIV] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Actividad1ProgIV] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [Actividad1ProgIV] SET QUERY_STORE = OFF
GO
USE [Actividad1ProgIV]
GO
/****** Object:  Table [dbo].[COLOR]    Script Date: 28/8/2021 12:10:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[COLOR](
	[ID_COLOR] [int] IDENTITY(1,1) NOT NULL,
	[Color] [nchar](10) NULL,
 CONSTRAINT [PK_dbo.COLOR] PRIMARY KEY CLUSTERED 
(
	[ID_COLOR] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MARCA]    Script Date: 28/8/2021 12:10:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MARCA](
	[ID_MARCA] [int] IDENTITY(1,1) NOT NULL,
	[MARCA] [nchar](20) NOT NULL,
 CONSTRAINT [PK_MARCA] PRIMARY KEY CLUSTERED 
(
	[ID_MARCA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AUTO]    Script Date: 28/8/2021 12:10:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AUTO](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PATENTE] [nchar](10) NOT NULL,
	[ID_MARCA] [int] NOT NULL,
	[ID_COLOR] [int] NOT NULL,
 CONSTRAINT [PK_AUTOMOVIL] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VistaGeneral]    Script Date: 28/8/2021 12:10:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VistaGeneral]
AS
SELECT        dbo.AUTO.ID, dbo.AUTO.PATENTE, dbo.COLOR.Color, dbo.MARCA.MARCA
FROM            dbo.AUTO INNER JOIN
                         dbo.COLOR ON dbo.AUTO.ID_COLOR = dbo.COLOR.ID_COLOR INNER JOIN
                         dbo.MARCA ON dbo.AUTO.ID_MARCA = dbo.MARCA.ID_MARCA
GO
SET IDENTITY_INSERT [dbo].[AUTO] ON 
GO
INSERT [dbo].[AUTO] ([ID], [PATENTE], [ID_MARCA], [ID_COLOR]) VALUES (1, N'AB123CD   ', 2, 1)
GO
INSERT [dbo].[AUTO] ([ID], [PATENTE], [ID_MARCA], [ID_COLOR]) VALUES (2, N'EF456GH   ', 7, 9)
GO
INSERT [dbo].[AUTO] ([ID], [PATENTE], [ID_MARCA], [ID_COLOR]) VALUES (3, N'AA421DD   ', 10, 3)
GO
INSERT [dbo].[AUTO] ([ID], [PATENTE], [ID_MARCA], [ID_COLOR]) VALUES (4, N'BC323BB   ', 1, 1)
GO
INSERT [dbo].[AUTO] ([ID], [PATENTE], [ID_MARCA], [ID_COLOR]) VALUES (5, N'DD323DE   ', 4, 5)
GO
INSERT [dbo].[AUTO] ([ID], [PATENTE], [ID_MARCA], [ID_COLOR]) VALUES (6, N'AA222AA   ', 3, 5)
GO
SET IDENTITY_INSERT [dbo].[AUTO] OFF
GO
SET IDENTITY_INSERT [dbo].[COLOR] ON 
GO
INSERT [dbo].[COLOR] ([ID_COLOR], [Color]) VALUES (1, N'ROJO      ')
GO
INSERT [dbo].[COLOR] ([ID_COLOR], [Color]) VALUES (2, N'AMARILLO  ')
GO
INSERT [dbo].[COLOR] ([ID_COLOR], [Color]) VALUES (3, N'AZUL      ')
GO
INSERT [dbo].[COLOR] ([ID_COLOR], [Color]) VALUES (4, N'VERDE     ')
GO
INSERT [dbo].[COLOR] ([ID_COLOR], [Color]) VALUES (5, N'BLANCO    ')
GO
INSERT [dbo].[COLOR] ([ID_COLOR], [Color]) VALUES (6, N'NEGRO     ')
GO
INSERT [dbo].[COLOR] ([ID_COLOR], [Color]) VALUES (7, N'GRIS      ')
GO
INSERT [dbo].[COLOR] ([ID_COLOR], [Color]) VALUES (8, N'MARRON    ')
GO
INSERT [dbo].[COLOR] ([ID_COLOR], [Color]) VALUES (9, N'NARANJA   ')
GO
SET IDENTITY_INSERT [dbo].[COLOR] OFF
GO
SET IDENTITY_INSERT [dbo].[MARCA] ON 
GO
INSERT [dbo].[MARCA] ([ID_MARCA], [MARCA]) VALUES (1, N'Ford                ')
GO
INSERT [dbo].[MARCA] ([ID_MARCA], [MARCA]) VALUES (2, N'BMW                 ')
GO
INSERT [dbo].[MARCA] ([ID_MARCA], [MARCA]) VALUES (3, N'Renault             ')
GO
INSERT [dbo].[MARCA] ([ID_MARCA], [MARCA]) VALUES (4, N'Audi                ')
GO
INSERT [dbo].[MARCA] ([ID_MARCA], [MARCA]) VALUES (5, N'Mercedes-Benz       ')
GO
INSERT [dbo].[MARCA] ([ID_MARCA], [MARCA]) VALUES (6, N'Honda               ')
GO
INSERT [dbo].[MARCA] ([ID_MARCA], [MARCA]) VALUES (7, N'Toyota              ')
GO
INSERT [dbo].[MARCA] ([ID_MARCA], [MARCA]) VALUES (8, N'Chevrolet           ')
GO
INSERT [dbo].[MARCA] ([ID_MARCA], [MARCA]) VALUES (9, N'Volkswagen          ')
GO
INSERT [dbo].[MARCA] ([ID_MARCA], [MARCA]) VALUES (10, N'Citroen             ')
GO
SET IDENTITY_INSERT [dbo].[MARCA] OFF
GO
ALTER TABLE [dbo].[AUTO]  WITH CHECK ADD  CONSTRAINT [FK_AUTO_COLOR] FOREIGN KEY([ID_COLOR])
REFERENCES [dbo].[COLOR] ([ID_COLOR])
GO
ALTER TABLE [dbo].[AUTO] CHECK CONSTRAINT [FK_AUTO_COLOR]
GO
ALTER TABLE [dbo].[AUTO]  WITH CHECK ADD  CONSTRAINT [FK_AUTO_MARCA] FOREIGN KEY([ID_MARCA])
REFERENCES [dbo].[MARCA] ([ID_MARCA])
GO
ALTER TABLE [dbo].[AUTO] CHECK CONSTRAINT [FK_AUTO_MARCA]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AUTO"
            Begin Extent = 
               Top = 66
               Left = 37
               Bottom = 196
               Right = 207
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "COLOR"
            Begin Extent = 
               Top = 159
               Left = 262
               Bottom = 255
               Right = 432
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "MARCA"
            Begin Extent = 
               Top = 44
               Left = 262
               Bottom = 140
               Right = 432
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VistaGeneral'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VistaGeneral'
GO
USE [master]
GO
ALTER DATABASE [Actividad1ProgIV] SET  READ_WRITE 
GO
