﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Personajes
{
    class HABILIDAD
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private string habilidad;

        public string Habilidad
        {
            get { return habilidad; }
            set { habilidad = value; }
        }

        public override string ToString()
        {
            return habilidad;
        }

        public void Insertar()
        {
            AccesoDatos acceso = new AccesoDatos();

            string sql = "SELECT ISNULL (MAX(ID_Habilidad),0) + 1 FROM HABILIDAD";

            this.id = acceso.DevolverEscalar(sql);

            sql = "SET IDENTITY_INSERT Habilidad ON; INSERT INTO HABILIDAD (ID_Habilidad, Habilidad) VALUES (" + this.id.ToString() + ", '" + this.Habilidad + "')";

            acceso.Escribir(sql);
        }
    }
}
