﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Personajes
{
    public partial class FRM_Raza : Form
    {
        public FRM_Raza()
        {
            InitializeComponent();
        }

        private void button_Cerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Agregar_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                RAZA raza = new RAZA();

                raza.Raza = textBox1.Text;
                raza.Insertar();
                AccesoDatos acceso = new AccesoDatos();
                acceso.ListarRazas();
                this.Close();
            }
            else
            {
                MessageBox.Show("No puedes crear una raza sin nombre!", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
