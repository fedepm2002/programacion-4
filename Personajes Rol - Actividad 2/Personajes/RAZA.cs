﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Personajes
{
    class RAZA
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private string raza;

        public string Raza
        {
            get { return raza; }
            set { raza = value; }
        }

        public override string ToString()
        {
            return raza;
        }

        public void Insertar()
        {
            AccesoDatos acceso = new AccesoDatos();

            string sql = "SELECT ISNULL (MAX(Raza_ID),0) + 1 FROM RAZA";

            this.id = acceso.DevolverEscalar(sql);

            sql = "SET IDENTITY_INSERT Raza ON; INSERT INTO RAZA (Raza_ID, Raza) VALUES (" + this.id.ToString() + ", '"+ this.Raza + "')";

            acceso.Escribir(sql);
        }
    }
}
