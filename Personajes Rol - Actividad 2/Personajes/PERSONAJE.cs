﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace Personajes
{
    class PERSONAJE
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private int fuerza;

        public int Fuerza
        {
            get { return fuerza; }
            set { fuerza = value; }
        }

        private int nivel;

        public int Nivel
        {
            get { return nivel; }
            set { nivel = value; }
        }

        private int puntos;

        public int Puntos
        {
            get { return puntos; }
            set { puntos = value; }
        }

        private int experiencia;

        public int Experiencia
        {
            get { return experiencia; }
            set { experiencia = value; }
        }

        private RAZA raza;

        public RAZA Raza
        {
            get { return raza; }
            set { raza = value; }
        }

        private List<HABILIDAD> habilidades = new List<HABILIDAD>();

        public List<HABILIDAD> Habilidades
        {
            get { return habilidades; }
        }

        public void CrearHabilidad(HABILIDAD habilidad)
        {
            string sql = "INSERT INTO PERSONAJES_HABILIDAD (ID_JUGADOR, id_habilidad) VALUES (" + this.id.ToString() + "," + habilidad.ID.ToString() + ")";

            AccesoDatos acceso = new AccesoDatos();

            if(acceso.Escribir(sql) == 1)
            {
                this.habilidades.Add(habilidad);
            }
        }

        public List<HABILIDAD> EliminarHabilidad(HABILIDAD habilidad)
        {
            string sql = "DELETE FROM PERSONAJES_HABILIDAD WHERE ID_HABILIDAD =" + habilidad.ID.ToString() + " AND ID_JUGADOR =" + this.id.ToString();

            AccesoDatos acceso = new AccesoDatos();

            if (acceso.Escribir(sql) == 1)
            {
                this.habilidades.Remove(habilidad);
            }
            return habilidades;
        }

        public void ObtenerHabilidades()
        {
            this.habilidades.Clear();

            string sql = "SELECT * FROM VistaHabilidad WHERE ID_JUGADOR = " + this.id.ToString();

            AccesoDatos acceso = new AccesoDatos();

            DataTable tabla = acceso.Listar(sql);

            foreach(DataRow registro in tabla.Rows)
            {
                HABILIDAD habilidad = new HABILIDAD();

                habilidad.ID = int.Parse(registro["ID_HABILIDAD"].ToString());

                habilidad.Habilidad = registro["Habilidad"].ToString();

                this.habilidades.Add(habilidad);
            }
        }
    }
}
