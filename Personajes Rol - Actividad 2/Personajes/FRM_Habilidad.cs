﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Personajes
{
    public partial class FRM_Habilidad : Form
    {
        public FRM_Habilidad()
        {
            InitializeComponent();
        }

        private void button_Cerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_Agregar_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                HABILIDAD habilidad = new HABILIDAD();

                habilidad.Habilidad = textBox1.Text;
                habilidad.Insertar();
                AccesoDatos acceso = new AccesoDatos();
                acceso.ListarHabilidades();
                this.Close();
            }
            else
            {
                MessageBox.Show("No puedes crear una habilidad sin nombre!", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
