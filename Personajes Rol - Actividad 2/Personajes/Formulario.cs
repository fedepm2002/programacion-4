﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Personajes
{
    public partial class Formulario : Form
    {
        AccesoDatos accesodatos = new AccesoDatos();
        public Formulario()
        {
            InitializeComponent();
        }

        private void Formulario_Activated(object sender, EventArgs e)
        {
            EnlazarPersonajes();
            EnlazarRazas();
            comboBox_Habilidades.DataSource = null;
            comboBox_Habilidades.DataSource = accesodatos.ListarHabilidades();
        }

        private void dataGridViewGeneral_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                PERSONAJE personaje = (PERSONAJE)dataGridViewGeneral.Rows[e.RowIndex].DataBoundItem;

                NombreLabel.Text = personaje.Nombre;
                textBox1.Text = personaje.Nombre.ToString().Replace(" ", String.Empty);
                textBox2.Text = personaje.Fuerza.ToString();
                textBox3.Text = personaje.Nivel.ToString();
                textBox4.Text = personaje.Puntos.ToString();
                textBox5.Text = personaje.Experiencia.ToString();

                foreach (object item in comboBox_Raza.Items)
                {
                    if (((RAZA)item).ID == personaje.Raza.ID)
                    {
                        comboBox_Raza.SelectedItem = item;
                    }
                }
                GC.Collect();
                EnlazarHabilidades(personaje);
            }
        }
        private void dataGridViewHabilidades_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                HABILIDAD habilidad = (HABILIDAD)dataGridViewHabilidades.Rows[e.RowIndex].DataBoundItem;
                foreach (object item in comboBox_Habilidades.Items)
                {
                    if (((HABILIDAD)item).ID == habilidad.ID)
                    {
                        comboBox_Habilidades.SelectedItem = item;
                    }
                }
            }
            GC.Collect();
        }

        void EnlazarPersonajes()
        {
            GC.Collect();
            dataGridViewGeneral.DataSource = null;
            dataGridViewGeneral.DataSource = accesodatos.ListarPersonajes();
        }
        void EnlazarRazas()
        {
            GC.Collect();
            comboBox_Raza.DataSource = null;
            comboBox_Raza.DataSource = accesodatos.ListarRazas();
        }
        void EnlazarHabilidades(PERSONAJE p)
        {
            p.ObtenerHabilidades();

            dataGridViewHabilidades.DataSource = null;
            if (p.Habilidades != null)
            {
                dataGridViewHabilidades.DataSource = p.Habilidades;
            }

            GC.Collect();
            comboBox_Habilidades.DataSource = null;
            comboBox_Habilidades.DataSource = accesodatos.ListarHabilidades();
        }

        private void NuevoPersonaje_Boton_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "" && textBox5.Text != "")
            {
                string sql = "INSERT INTO PERSONAJE (NOMBRE, FUERZA, NIVEL, PUNTOS, EXPERIENCIA, ID_RAZA) values ('" + textBox1.Text + "', " + textBox2.Text + ", " + textBox3.Text + ", " + textBox4.Text + ", " + textBox5.Text + ", " + (comboBox_Raza.SelectedIndex + 1) + ")";

                if (accesodatos.Escribir(sql) == -1)
                {
                    MessageBox.Show("Ocurrió un error al crear el personaje.", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Se ha creado el personaje \"" + textBox1.Text.Replace(" ", String.Empty) + "\"", "Personaje Creado!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                GC.Collect();
                EnlazarPersonajes();
            }
            else
            {
                MessageBox.Show("No puedes crear un personaje con valores vacíos!", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void NuevaRaza_Boton_Click(object sender, EventArgs e)
        {
            FRM_Raza frm_raza = new FRM_Raza();

            frm_raza.ShowDialog();
            GC.Collect();
        }

        private void NuevaHabilidad_Boton_Click(object sender, EventArgs e)
        {
            FRM_Habilidad frm_habilidad = new FRM_Habilidad();

            frm_habilidad.ShowDialog();
            GC.Collect();
        }

        private void Editar_Boton_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "" && textBox5.Text != "")
            {
                RAZA raza = (RAZA)comboBox_Raza.SelectedItem;

                string sql = "UPDATE PERSONAJE SET NOMBRE ='" + textBox1.Text;
                sql += "', FUERZA =" + textBox2.Text + ", NIVEL =" + textBox3.Text + ", PUNTOS =" + textBox4.Text + ", EXPERIENCIA =" + textBox5.Text;
                sql += ", ID_RAZA =" + raza.ID.ToString() + " WHERE NOMBRE= '" + NombreLabel.Text + "'";

                if (accesodatos.Escribir(sql) == -1 && dataGridViewGeneral.SelectedRows.Count > 0)
                {
                    MessageBox.Show("Ocurrió un error al editar el personaje.", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (dataGridViewGeneral.SelectedRows.Count == 0)
                {
                    MessageBox.Show("Debes seleccionar un personaje en la lista!", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Se ha editado el personaje \"" + NombreLabel.Text.Replace(" ", String.Empty) + "\"", "Personaje Editado!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                GC.Collect();
                EnlazarPersonajes();
            }
            else
            {
                MessageBox.Show("No puedes editar un personaje con valores vacíos!", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Borrar_Boton_Click(object sender, EventArgs e)
        {
            if (dataGridViewGeneral.SelectedRows.Count != 0) {
                foreach (DataGridViewRow registro in dataGridViewGeneral.SelectedRows)
                {
                    PERSONAJE personaje = (PERSONAJE)registro.DataBoundItem;

                    string sql = "DELETE FROM PERSONAJES_HABILIDAD WHERE ID_JUGADOR =" + personaje.ID.ToString();
                    sql += "; DELETE FROM PERSONAJE WHERE ID =" + personaje.ID.ToString();

                    if (accesodatos.Escribir(sql) == -1)
                    {
                        MessageBox.Show("Ocurrió un error al borrar el personaje.", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Se ha borrado el personaje \"" + NombreLabel.Text.Replace(" ", String.Empty) + "\"", "Personaje Borrado!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            else
            {
                MessageBox.Show("Debes seleccionar un personaje en la lista!", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            GC.Collect();
            dataGridViewGeneral.DataSource = null;
            dataGridViewHabilidades.DataSource = null;
            EnlazarPersonajes();
        }

        private void AsignarHabilidad_Boton_Click(object sender, EventArgs e)
        {
            if (dataGridViewGeneral.SelectedRows.Count >= 0 && dataGridViewGeneral.SelectedRows.Count != 0)
            {
                PERSONAJE personaje = (PERSONAJE)dataGridViewGeneral.SelectedRows[0].DataBoundItem;

                HABILIDAD habilidad = (HABILIDAD)comboBox_Habilidades.SelectedItem;

                personaje.CrearHabilidad(habilidad);

                EnlazarHabilidades(personaje);
            }
            else
            {
                MessageBox.Show("Debes seleccionar un personaje en la lista!", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            GC.Collect();
        }

        private void DesasignarHabilidad_Boton_Click(object sender, EventArgs e)
        {
            if (dataGridViewGeneral.SelectedRows.Count >= 0 && dataGridViewHabilidades.SelectedRows.Count != 0)
            {
                PERSONAJE personaje = (PERSONAJE)dataGridViewGeneral.SelectedRows[0].DataBoundItem;

                HABILIDAD habilidad = (HABILIDAD)dataGridViewHabilidades.SelectedRows[0].DataBoundItem;

                if (personaje.EliminarHabilidad(habilidad).Count < 1)
                {
                    dataGridViewHabilidades.DataSource = null;
                }

                EnlazarHabilidades(personaje);
            }
            else
            {
                MessageBox.Show("Debes seleccionar una habilidad en la lista!", "ERROR!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            GC.Collect();
        }
    }
}
