﻿
namespace Personajes
{
    partial class Formulario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Formulario));
            this.Borrar_Boton = new System.Windows.Forms.Button();
            this.Editar_Boton = new System.Windows.Forms.Button();
            this.NuevoPersonaje_Boton = new System.Windows.Forms.Button();
            this.NuevaHabilidad_Boton = new System.Windows.Forms.Button();
            this.NuevaRaza_Boton = new System.Windows.Forms.Button();
            this.dataGridViewGeneral = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.NombreLabel = new System.Windows.Forms.Label();
            this.comboBox_Raza = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dataGridViewHabilidades = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.DesasignarHabilidad_Boton = new System.Windows.Forms.Button();
            this.AsignarHabilidad_Boton = new System.Windows.Forms.Button();
            this.comboBox_Habilidades = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGeneral)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHabilidades)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // Borrar_Boton
            // 
            this.Borrar_Boton.BackColor = System.Drawing.Color.DarkRed;
            this.Borrar_Boton.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.Borrar_Boton.ForeColor = System.Drawing.Color.White;
            this.Borrar_Boton.Location = new System.Drawing.Point(172, 292);
            this.Borrar_Boton.Name = "Borrar_Boton";
            this.Borrar_Boton.Size = new System.Drawing.Size(101, 46);
            this.Borrar_Boton.TabIndex = 11;
            this.Borrar_Boton.Text = "BORRAR";
            this.Borrar_Boton.UseVisualStyleBackColor = false;
            this.Borrar_Boton.Click += new System.EventHandler(this.Borrar_Boton_Click);
            // 
            // Editar_Boton
            // 
            this.Editar_Boton.BackColor = System.Drawing.Color.DarkOrange;
            this.Editar_Boton.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.Editar_Boton.ForeColor = System.Drawing.Color.Black;
            this.Editar_Boton.Location = new System.Drawing.Point(56, 291);
            this.Editar_Boton.Name = "Editar_Boton";
            this.Editar_Boton.Size = new System.Drawing.Size(101, 47);
            this.Editar_Boton.TabIndex = 10;
            this.Editar_Boton.Text = "EDITAR";
            this.Editar_Boton.UseVisualStyleBackColor = false;
            this.Editar_Boton.Click += new System.EventHandler(this.Editar_Boton_Click);
            // 
            // NuevoPersonaje_Boton
            // 
            this.NuevoPersonaje_Boton.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.NuevoPersonaje_Boton.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.NuevoPersonaje_Boton.ForeColor = System.Drawing.Color.DarkBlue;
            this.NuevoPersonaje_Boton.Location = new System.Drawing.Point(90, 225);
            this.NuevoPersonaje_Boton.Name = "NuevoPersonaje_Boton";
            this.NuevoPersonaje_Boton.Size = new System.Drawing.Size(139, 61);
            this.NuevoPersonaje_Boton.TabIndex = 9;
            this.NuevoPersonaje_Boton.Text = "NUEVO PERSONAJE";
            this.NuevoPersonaje_Boton.UseVisualStyleBackColor = false;
            this.NuevoPersonaje_Boton.Click += new System.EventHandler(this.NuevoPersonaje_Boton_Click);
            // 
            // NuevaHabilidad_Boton
            // 
            this.NuevaHabilidad_Boton.AutoSize = true;
            this.NuevaHabilidad_Boton.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.NuevaHabilidad_Boton.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.NuevaHabilidad_Boton.ForeColor = System.Drawing.Color.DarkBlue;
            this.NuevaHabilidad_Boton.Location = new System.Drawing.Point(56, 138);
            this.NuevaHabilidad_Boton.Name = "NuevaHabilidad_Boton";
            this.NuevaHabilidad_Boton.Size = new System.Drawing.Size(217, 29);
            this.NuevaHabilidad_Boton.TabIndex = 8;
            this.NuevaHabilidad_Boton.Text = "CREAR NUEVA HABILIDAD";
            this.NuevaHabilidad_Boton.UseVisualStyleBackColor = false;
            this.NuevaHabilidad_Boton.Click += new System.EventHandler(this.NuevaHabilidad_Boton_Click);
            // 
            // NuevaRaza_Boton
            // 
            this.NuevaRaza_Boton.AutoSize = true;
            this.NuevaRaza_Boton.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.NuevaRaza_Boton.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.NuevaRaza_Boton.ForeColor = System.Drawing.Color.DarkBlue;
            this.NuevaRaza_Boton.Location = new System.Drawing.Point(28, 190);
            this.NuevaRaza_Boton.Name = "NuevaRaza_Boton";
            this.NuevaRaza_Boton.Size = new System.Drawing.Size(265, 29);
            this.NuevaRaza_Boton.TabIndex = 6;
            this.NuevaRaza_Boton.Text = "CREAR NUEVA RAZA";
            this.NuevaRaza_Boton.UseVisualStyleBackColor = false;
            this.NuevaRaza_Boton.Click += new System.EventHandler(this.NuevaRaza_Boton_Click);
            // 
            // dataGridViewGeneral
            // 
            this.dataGridViewGeneral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewGeneral.Location = new System.Drawing.Point(11, 12);
            this.dataGridViewGeneral.Name = "dataGridViewGeneral";
            this.dataGridViewGeneral.RowHeadersWidth = 30;
            this.dataGridViewGeneral.RowTemplate.Height = 25;
            this.dataGridViewGeneral.Size = new System.Drawing.Size(911, 344);
            this.dataGridViewGeneral.TabIndex = 7;
            this.dataGridViewGeneral.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewGeneral_CellClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.NombreLabel);
            this.groupBox1.Controls.Add(this.comboBox_Raza);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.Borrar_Boton);
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.Editar_Boton);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.NuevaRaza_Boton);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.NuevoPersonaje_Boton);
            this.groupBox1.Location = new System.Drawing.Point(928, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(325, 345);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Personalizar Personaje";
            // 
            // NombreLabel
            // 
            this.NombreLabel.AutoSize = true;
            this.NombreLabel.Location = new System.Drawing.Point(250, 31);
            this.NombreLabel.Name = "NombreLabel";
            this.NombreLabel.Size = new System.Drawing.Size(63, 13);
            this.NombreLabel.TabIndex = 22;
            this.NombreLabel.Text = "Placeholder";
            this.NombreLabel.Visible = false;
            // 
            // comboBox_Raza
            // 
            this.comboBox_Raza.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Raza.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.comboBox_Raza.FormattingEnabled = true;
            this.comboBox_Raza.Location = new System.Drawing.Point(28, 159);
            this.comboBox_Raza.Name = "comboBox_Raza";
            this.comboBox_Raza.Size = new System.Drawing.Size(265, 25);
            this.comboBox_Raza.TabIndex = 21;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 143);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Raza";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(238, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 19;
            this.label5.Text = "Experiencia";
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(245, 97);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(48, 35);
            this.textBox5.TabIndex = 18;
            this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(167, 81);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "Puntos";
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(163, 97);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(48, 35);
            this.textBox4.TabIndex = 16;
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(98, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Nivel";
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(90, 97);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(48, 35);
            this.textBox3.TabIndex = 14;
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Fuerza";
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(21, 97);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(48, 35);
            this.textBox2.TabIndex = 12;
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Nombre";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(6, 47);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(307, 23);
            this.textBox1.TabIndex = 10;
            // 
            // dataGridViewHabilidades
            // 
            this.dataGridViewHabilidades.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewHabilidades.Location = new System.Drawing.Point(11, 363);
            this.dataGridViewHabilidades.Name = "dataGridViewHabilidades";
            this.dataGridViewHabilidades.RowTemplate.Height = 25;
            this.dataGridViewHabilidades.Size = new System.Drawing.Size(911, 317);
            this.dataGridViewHabilidades.TabIndex = 13;
            this.dataGridViewHabilidades.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewHabilidades_CellClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.DesasignarHabilidad_Boton);
            this.groupBox2.Controls.Add(this.AsignarHabilidad_Boton);
            this.groupBox2.Controls.Add(this.comboBox_Habilidades);
            this.groupBox2.Controls.Add(this.NuevaHabilidad_Boton);
            this.groupBox2.Location = new System.Drawing.Point(928, 363);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(325, 317);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Habilidades";
            // 
            // DesasignarHabilidad_Boton
            // 
            this.DesasignarHabilidad_Boton.BackColor = System.Drawing.Color.DarkRed;
            this.DesasignarHabilidad_Boton.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Bold);
            this.DesasignarHabilidad_Boton.ForeColor = System.Drawing.Color.White;
            this.DesasignarHabilidad_Boton.Location = new System.Drawing.Point(7, 96);
            this.DesasignarHabilidad_Boton.Name = "DesasignarHabilidad_Boton";
            this.DesasignarHabilidad_Boton.Size = new System.Drawing.Size(312, 36);
            this.DesasignarHabilidad_Boton.TabIndex = 23;
            this.DesasignarHabilidad_Boton.Text = "DESASIGNAR HABILIDAD";
            this.DesasignarHabilidad_Boton.UseVisualStyleBackColor = false;
            this.DesasignarHabilidad_Boton.Click += new System.EventHandler(this.DesasignarHabilidad_Boton_Click);
            // 
            // AsignarHabilidad_Boton
            // 
            this.AsignarHabilidad_Boton.AutoSize = true;
            this.AsignarHabilidad_Boton.BackColor = System.Drawing.Color.LimeGreen;
            this.AsignarHabilidad_Boton.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.AsignarHabilidad_Boton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.AsignarHabilidad_Boton.Location = new System.Drawing.Point(7, 61);
            this.AsignarHabilidad_Boton.Name = "AsignarHabilidad_Boton";
            this.AsignarHabilidad_Boton.Size = new System.Drawing.Size(312, 29);
            this.AsignarHabilidad_Boton.TabIndex = 9;
            this.AsignarHabilidad_Boton.Text = "ASIGNAR HABILIDAD A PERSONAJE";
            this.AsignarHabilidad_Boton.UseVisualStyleBackColor = false;
            this.AsignarHabilidad_Boton.Click += new System.EventHandler(this.AsignarHabilidad_Boton_Click);
            // 
            // comboBox_Habilidades
            // 
            this.comboBox_Habilidades.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_Habilidades.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_Habilidades.FormattingEnabled = true;
            this.comboBox_Habilidades.Location = new System.Drawing.Point(7, 30);
            this.comboBox_Habilidades.Name = "comboBox_Habilidades";
            this.comboBox_Habilidades.Size = new System.Drawing.Size(312, 25);
            this.comboBox_Habilidades.TabIndex = 0;
            // 
            // Formulario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1265, 691);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dataGridViewHabilidades);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGridViewGeneral);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Formulario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Character Customizer";
            this.Activated += new System.EventHandler(this.Formulario_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGeneral)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewHabilidades)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Borrar_Boton;
        private System.Windows.Forms.Button Editar_Boton;
        private System.Windows.Forms.Button NuevoPersonaje_Boton;
        private System.Windows.Forms.Button NuevaHabilidad_Boton;
        private System.Windows.Forms.Button NuevaRaza_Boton;
        private System.Windows.Forms.DataGridView dataGridViewGeneral;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.ComboBox comboBox_Raza;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dataGridViewHabilidades;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button AsignarHabilidad_Boton;
        private System.Windows.Forms.ComboBox comboBox_Habilidades;
        private System.Windows.Forms.Button DesasignarHabilidad_Boton;
        private System.Windows.Forms.Label NombreLabel;
    }
}