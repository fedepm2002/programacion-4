﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Personajes
{
    class AccesoDatos
    {
        private SqlConnection conexion;

        public AccesoDatos()
        {
            conexion = new SqlConnection();

            conexion.ConnectionString = @"Data source=.\sqlexpress; Initial Catalog=Personajes; Integrated Security= SSPI;";
        }

        public void Abrir()
        {
            conexion.Open();
        }

        public void Cerrar()
        {
            conexion.Close();
        }

        public int Escribir(string sql)
        {
            int filasAfectadas = 0;
            Abrir();

            SqlCommand comando = CrearComando(sql);

            try
            {
                filasAfectadas = comando.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                filasAfectadas = -1;
            }
            Cerrar();
            return filasAfectadas;
        }

        public SqlCommand CrearComando(string sql)
        {
            SqlCommand comando = new SqlCommand();

            comando.CommandText = sql;

            comando.Connection = conexion;

            comando.CommandType = CommandType.Text;

            return comando;
        }

        public List<RAZA> ListarRazas()
        {
            List<RAZA> razas = new List<RAZA>();

            Abrir();

            string sql = "SELECT * FROM RAZA";

            SqlCommand comando = CrearComando(sql);

            SqlDataReader lector = comando.ExecuteReader();

            while (lector.Read())
            {
                RAZA color = new RAZA();
                color.ID = int.Parse(lector["raza_id"].ToString());
                color.Raza = lector["Raza"].ToString();

                razas.Add(color);
            }

            lector.Close();

            Cerrar();
            return razas;
        }

        public List<HABILIDAD> ListarHabilidades()
        {
            List<HABILIDAD> habilidades = new List<HABILIDAD>();

            Abrir();

            string sql = "SELECT * FROM HABILIDAD";

            SqlCommand comando = CrearComando(sql);

            SqlDataReader lector = comando.ExecuteReader();

            while (lector.Read())
            {
                HABILIDAD habilidad = new HABILIDAD();
                habilidad.ID = int.Parse(lector["id_habilidad"].ToString());
                habilidad.Habilidad = lector["Habilidad"].ToString();

                habilidades.Add(habilidad);
            }

            lector.Close();

            Cerrar();
            return habilidades;
        }

        public List<PERSONAJE> ListarPersonajes()
        {
            List<PERSONAJE> personajes = new List<PERSONAJE>();

            List<RAZA> razas = ListarRazas();

            Abrir();

            string sql = "SELECT ID, NOMBRE, FUERZA, NIVEL, PUNTOS, EXPERIENCIA, ID_RAZA FROM PERSONAJE";
            SqlCommand comando = CrearComando(sql);

            SqlDataReader lector = comando.ExecuteReader();

            while (lector.Read())
            {
                PERSONAJE personaje = new PERSONAJE();
                personaje.ID = int.Parse(lector["ID"].ToString());
                personaje.Nombre = lector["NOMBRE"].ToString();
                personaje.Fuerza = int.Parse(lector["FUERZA"].ToString());
                personaje.Nivel = int.Parse(lector["NIVEL"].ToString());
                personaje.Puntos = int.Parse(lector["PUNTOS"].ToString());
                personaje.Experiencia = int.Parse(lector["EXPERIENCIA"].ToString());
                int indice = 0;

                while (personaje.Raza == null)
                {
                    if (int.Parse(lector["ID_RAZA"].ToString()) == razas[indice].ID)
                    {
                        personaje.Raza = razas[indice];
                    }
                    else
                    {
                        indice++;
                    }
                }

                personajes.Add(personaje);
            }

            lector.Close();
            Cerrar();
            return personajes;
        }

        public int DevolverEscalar(string sql)
        {
            Abrir();

            int id;

            SqlCommand comando = CrearComando(sql);

            id = int.Parse(comando.ExecuteScalar().ToString());

            Cerrar();
            return id;
        }

        public DataTable Listar(string sql)
        {
            DataTable tabla = new DataTable();

            SqlDataAdapter adaptador = new SqlDataAdapter();

            adaptador.SelectCommand = CrearComando(sql);

            adaptador.Fill(tabla);

            adaptador = null;

            return tabla;
        }
    }
}
