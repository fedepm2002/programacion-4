USE [master]
GO
/****** Object:  Database [Personajes]    Script Date: 11/9/2021 14:52:55 ******/
CREATE DATABASE [Personajes]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Personajes', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Personajes.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Personajes_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\Personajes_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [Personajes] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Personajes].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Personajes] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Personajes] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Personajes] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Personajes] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Personajes] SET ARITHABORT OFF 
GO
ALTER DATABASE [Personajes] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Personajes] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Personajes] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Personajes] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Personajes] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Personajes] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Personajes] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Personajes] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Personajes] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Personajes] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Personajes] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Personajes] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Personajes] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Personajes] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Personajes] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Personajes] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Personajes] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Personajes] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Personajes] SET  MULTI_USER 
GO
ALTER DATABASE [Personajes] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Personajes] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Personajes] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Personajes] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Personajes] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Personajes] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [Personajes] SET QUERY_STORE = OFF
GO
USE [Personajes]
GO
/****** Object:  Table [dbo].[Raza]    Script Date: 11/9/2021 14:52:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Raza](
	[Raza_ID] [int] IDENTITY(1,1) NOT NULL,
	[Raza] [nchar](30) NOT NULL,
 CONSTRAINT [PK_Raza] PRIMARY KEY CLUSTERED 
(
	[Raza_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Habilidad]    Script Date: 11/9/2021 14:52:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Habilidad](
	[ID_Habilidad] [int] IDENTITY(1,1) NOT NULL,
	[Habilidad] [nchar](30) NOT NULL,
 CONSTRAINT [PK_Habilidad] PRIMARY KEY CLUSTERED 
(
	[ID_Habilidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Personaje]    Script Date: 11/9/2021 14:52:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Personaje](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nchar](40) NOT NULL,
	[Fuerza] [int] NOT NULL,
	[Nivel] [int] NOT NULL,
	[Puntos] [int] NOT NULL,
	[Experiencia] [int] NOT NULL,
	[ID_Raza] [int] NOT NULL,
 CONSTRAINT [PK_Personaje] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VistaGeneral]    Script Date: 11/9/2021 14:52:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VistaGeneral]
AS
SELECT        dbo.Personaje.Nombre, dbo.Personaje.Fuerza, dbo.Personaje.Nivel, dbo.Personaje.Puntos, dbo.Personaje.Experiencia, dbo.Raza.Raza, dbo.Habilidad.Habilidad
FROM            dbo.Personaje INNER JOIN
                         dbo.Raza ON dbo.Personaje.Raza = dbo.Raza.Raza_ID CROSS JOIN
                         dbo.Habilidad
GO
/****** Object:  Table [dbo].[Personajes_Habilidad]    Script Date: 11/9/2021 14:52:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Personajes_Habilidad](
	[ID_JUGADOR] [int] NOT NULL,
	[ID_HABILIDAD] [int] NOT NULL,
 CONSTRAINT [PK_Personajes_Habilidad] PRIMARY KEY CLUSTERED 
(
	[ID_JUGADOR] ASC,
	[ID_HABILIDAD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[VistaHabilidad]    Script Date: 11/9/2021 14:52:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VistaHabilidad]
AS
SELECT        dbo.Personajes_Habilidad.ID_JUGADOR, dbo.Habilidad.Habilidad, dbo.Personajes_Habilidad.ID_HABILIDAD
FROM            dbo.Habilidad INNER JOIN
                         dbo.Personajes_Habilidad ON dbo.Habilidad.ID_Habilidad = dbo.Personajes_Habilidad.ID_HABILIDAD
GO
ALTER TABLE [dbo].[Personaje]  WITH CHECK ADD  CONSTRAINT [FK_Personaje_Raza] FOREIGN KEY([ID_Raza])
REFERENCES [dbo].[Raza] ([Raza_ID])
GO
ALTER TABLE [dbo].[Personaje] CHECK CONSTRAINT [FK_Personaje_Raza]
GO
ALTER TABLE [dbo].[Personajes_Habilidad]  WITH CHECK ADD  CONSTRAINT [FK_Personajes_Habilidad_Habilidad] FOREIGN KEY([ID_HABILIDAD])
REFERENCES [dbo].[Habilidad] ([ID_Habilidad])
GO
ALTER TABLE [dbo].[Personajes_Habilidad] CHECK CONSTRAINT [FK_Personajes_Habilidad_Habilidad]
GO
ALTER TABLE [dbo].[Personajes_Habilidad]  WITH CHECK ADD  CONSTRAINT [FK_Personajes_Habilidad_Personaje] FOREIGN KEY([ID_JUGADOR])
REFERENCES [dbo].[Personaje] ([ID])
GO
ALTER TABLE [dbo].[Personajes_Habilidad] CHECK CONSTRAINT [FK_Personajes_Habilidad_Personaje]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[42] 4[20] 2[3] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Personaje"
            Begin Extent = 
               Top = 36
               Left = 444
               Bottom = 166
               Right = 614
            End
            DisplayFlags = 280
            TopColumn = 3
         End
         Begin Table = "Raza"
            Begin Extent = 
               Top = 90
               Left = 742
               Bottom = 186
               Right = 912
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Habilidad"
            Begin Extent = 
               Top = 51
               Left = 152
               Bottom = 152
               Right = 337
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 900
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VistaGeneral'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VistaGeneral'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[20] 2[10] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Habilidad"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 102
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Personajes_Habilidad"
            Begin Extent = 
               Top = 6
               Left = 454
               Bottom = 102
               Right = 624
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 2415
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VistaHabilidad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'VistaHabilidad'
GO
USE [master]
GO
ALTER DATABASE [Personajes] SET  READ_WRITE 
GO
